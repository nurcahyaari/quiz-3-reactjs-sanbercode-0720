import React from 'react';

import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import Home from './ui/pages/index';
import About from './ui/pages/about';
import Contact from './ui/pages/contact';
import Login from './ui/pages/login'
import MovieEditor from './ui/pages/movie-editor';
import PrivateRoute from './ui/components/Private_route';

import { AuthProvider } from './modules/context/auth.context';

function App() {
  return (
    <AuthProvider>
      <div className="App">
        <Router>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/about" exact component={About} />
            <Route path="/contact" exact component={Contact} />
            <Route path="/login" exact component={Login} />
            <PrivateRoute path="/movie-editor">
              <MovieEditor />
            </PrivateRoute>
          </Switch>
        </Router>
      </div>
    </AuthProvider>
  );
}

export default App;
