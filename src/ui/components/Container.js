import React from 'react';
import Header from './Header';
import Footer from './Footer';

/**
 * 
 * @param {any} props 
 */
const Container = (props) => {
    const { children } = props;
    return (
        <section >
            <Header />
            {children}
            <Footer />
        </section>
    );
}

export default Container;
