import React from 'react';
import Logo from '../../assets/imgs/Pekan-1_hari-ke-2_public_img_logo.png'

import { useAuthContext } from '../../modules/context/auth.context';

const Header = () => {
    const { auth } = useAuthContext();
    return (
        <header>
            <img id="logo" src={Logo} width="200px" />
            <nav>
                <ul>
                <li><a href="/">Home </a> </li>
                <li><a href="/about">About </a> </li>
                <li><a href="/contact">Contact </a> </li>
                {
                    auth.isLoggedIn ? (
                        <li><a href="/movie-editor">Movie Editor </a> </li>
                    ) : (
                        <li><a href="/login">Login </a> </li>
                    )
                }
                </ul>
            </nav>
        </header>
    )
}

export default Header;
