import React from 'react';

import { Route, Redirect } from 'react-router-dom';

import { useAuthContext } from '../../modules/context/auth.context';

const PrivateRoute = ({children, ...rest}) => {
    const { auth } = useAuthContext();
    console.log(children);
    return (
        <Route
            {...rest}
            render={({ location }) =>
            auth.isLoggedIn ? (
                children
            ) : (
                <Redirect
                    to={{
                        pathname: "/login",
                        state: { from: location }
                    }}
                />
            )
            }
        />
    )
}

export default PrivateRoute;
