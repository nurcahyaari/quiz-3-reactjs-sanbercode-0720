import React from 'react';

import Container from '../components/Container';
import Footer from '../components/Footer';
import Header from '../components/Header';

const About = () => {
    return (
        <Container>
            <>
                <div style={{padding: '10px', border: '1px solid #ccc'}}>
                    <h1 style={{textAlign: 'center'}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                    <ol>
                        <li><strong style={{width: '100px'}}>Nama:</strong> Ari Nurcahya</li> 
                        <li><strong style={{width: '100px'}}>Email:</strong> nurcahyaari@gmail.com</li> 
                        <li><strong style={{width: '100px'}}>Sistem Operasi yang digunakan:</strong> macOS</li>
                        <li><strong style={{width: '100px'}}>Akun Gitlab:</strong> @nurcahyaari</li> 
                        <li><strong style={{width: '100px'}}>Akun Telegram:</strong> @nurcahyaari</li> 
                    </ol>
                </div>
            </>
        </Container>

    )
}

export default About;
