import React, { useState, useEffect } from 'react'

import Container from '../components/Container';
import Footer from '../components/Footer';
import Header from '../components/Header';


const Home = () => {
    const [movies, setmovies] = useState([]);
    const init = async () => {
        const res = await fetch('http://backendexample.sanbercloud.com/api/movies');
        const json = await res.json();

        
        const sortedJson = json.sort((a, b) => b.rating - a.rating);

        setmovies(sortedJson);
    }

    useEffect(() => {
        init();
    }, []);

    return (
        <Container>
            <>
                <Header/>
                <h1>Featured Posts</h1>
                    <div id="article-list">
                        {
                            movies.length > 0 ? movies.map((movie) => (
                                <div key={movie.id}>
                                    <a href=""><h3>{movie.title}</h3></a>
                                    <p>
                                        {movie.description}
                                    </p>
                                </div>
                            )) : <></>
                        }
                    </div>
                <Footer/>
            </>
        </Container>
    )
}

export default Home;
