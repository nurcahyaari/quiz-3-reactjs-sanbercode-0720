import React, { useState } from 'react';

import Container from '../components/Container';

import { useAuthContext, loginSuccess, loginFail} from '../../modules/context/auth.context';
const Login = () => {
    const {auth, dispatch} = useAuthContext();
    const [username, setusername] = useState('');
    const [password, setpassword] = useState('');
    console.log(auth);  
    const login = () => {
        if (username === '') {
            dispatch(loginFail('Username tidak boleh kosong'));
            return;
        }
        dispatch(loginSuccess(username));
    }
    return (
        <Container>
            <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column'}}>
                <div style={{color: 'red'}}>
                    {auth.error}
                </div>
                <h1>Login</h1>
                <div>
                    <div>
                        <label>Username</label>
                        <input type="text" value={username} onChange={(e) => setusername(e.target.value)}/>
                    </div>
                    <div>
                        <label>Password</label>
                        <input type="password" value={password} onChange={(e) => setpassword(e.target.value)}/>
                    </div>
                    <div>
                        <button onClick={login}>Login</button>
                    </div>
                </div>
            </div>
        </Container>
    )
};

export default Login;
