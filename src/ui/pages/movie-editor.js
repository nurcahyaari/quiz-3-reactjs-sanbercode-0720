import React, {useState, useCallback, useEffect} from 'react';

import Container from '../components/Container';

const MovieEditor = () => {
    const [id, setid] = useState('');
    const [title, settittle] = useState();
    const [description, setdescription] = useState();
    const [year, setyear] = useState();
    const [duration, setduration] = useState();
    const [genre, setgenre] = useState();
    const [rating, setrating] = useState();

    const [movies, setmovies]  = useState([]);

    const init = async () => {
        const res = await fetch('http://backendexample.sanbercloud.com/api/movies');
        const json = await res.json();

        setmovies(json);
    }

    useEffect(() => {
        init();
    }, []);

    const handleRemove = async (id) => {
        const res = await fetch(
            `${'https://cors-anywhere.herokuapp.com/'}http://backendexample.sanbercloud.com/api/movies/${id}`,
            {
                method: 'DELETE',
            }
        );
        const json = await res.text();

        console.log(json);

        const newMovies = movies.filter((item) => item.id !== id);
        setmovies(newMovies);
    };
    
    const handleChangeMovie = async (id = '') => {
        if (id === '') {
            const res = await fetch(
                `${'https://cors-anywhere.herokuapp.com/'}http://backendexample.sanbercloud.com/api/movies`,
                {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body: JSON.stringify({
                        title,
                        year,
                        duration,
                        genre,
                        rating,
                        description,
                    }),
                }
            );
            const json = await res.json();
            
            setmovies([
                ...movies,
                json,
            ]);
            console.log(movies);
            return;
        }
        const res = await fetch(
            `${'https://cors-anywhere.herokuapp.com/'}http://backendexample.sanbercloud.com/api/movies/${id}`,
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body: JSON.stringify({
                    title,
                    year,
                    duration,
                    genre,
                    rating,
                    description,
                }),
            }
        );
        const json = await res.json();
        console.log(json);

        const newMoview = movies.map((movie) => {
            if (movie.id === json.id) {
                movie.title = json.title;
                movie.description = json.description;
                movie.year = json.year;
                movie.genre = json.genre;
                movie.rating = json.rating;
                movie.duration = json.duration;
            }
            return movie;
        })
        setmovies(newMoview);
        // buah.push({
        //     id: buah.length + 1,
        //     ...data,
        // });
        // setBuah(buah);
    
    }
    
    const setDetail = useCallback((item) => () => {
        setid(item.id);
        settittle(item.title);
        setdescription(item.description);
        setduration(item.duration);
        setgenre(item.genre);
        setyear(item.year);
        setrating(item.rating);
    });

    const unsetDetail = () => {
        setid('');
        settittle('');
        setdescription('');
        setduration('');
        setgenre('');
        setyear('');
        setrating('');
    }

    const saveMovie = (e) => {
        e.preventDefault();
        handleChangeMovie(id);
        unsetDetail();
    }

    return (
        <Container>
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                <h1>Table Movies</h1>
                <table style={{border: '1px solid black'}}>
                    <thead style={{ backgroundColor: 'grey' }}>
                        <th style={{width: 300}}>Judul</th>
                        <th style={{width: 200}}>Genre</th>
                        <th style={{width: 200}}>Rating</th>
                        <th style={{width: 200}}>Tahun</th>
                        <th style={{width: 200}}>Dureasi</th>
                        <th>#</th>
                    </thead>
                    <tbody>
                    {
                        movies.map((item) => (
                            <tr key={item.id} style={{ backgroundColor: 'salmon' }}>
                                <td>{item.title}</td>
                                <td>{item.genre}</td>
                                <td>{item.rating}</td>
                                <td>{item.year}</td>
                                <td>{item.duration}</td>
                                <td>
                                    <button onClick={setDetail(item)}>Edit</button>
                                    <button onClick={() => handleRemove(item.id)}>Hapus</button>
                                </td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
                <div style={{paddingTop: 30}}>
                    <h2>Edit Movie</h2>
                    <form method="POST">
                        <div>
                            <label>TItle</label>
                            <input type="text" value={title} onChange={(e) => settittle(e.target.value)}/>
                        </div>
                        <div>
                            <label>Description</label>
                            <textarea value={description} onChange={(e) => setdescription(e.target.value)}></textarea>
                        </div>
                        <div>
                            <label>Year</label>
                            <input type="number" value={year} onChange={(e) => setyear(e.target.value)}/>
                        </div>
                        <div>
                            <label>Duartion</label>
                            <input type="number" value={duration} onChange={(e) => setduration(e.target.value)}/>
                        </div>
                        <div>
                            <label>Genre</label>
                            <input type="text" value={genre} onChange={(e) => setgenre(e.target.value)}/>
                        </div>
                        <div>
                            <label>Rating</label>
                            <input type="number" value={rating} onChange={(e) => setrating(e.target.value)}/>
                        </div>
                        <div>
                            <input type="button" value="cancel" onClick={unsetDetail}/>
                            <input type="submit" value="Save" onClick={saveMovie}/>
                        </div>
                    </form>
                </div>
            </div>
        </Container>
    )
}

export default MovieEditor;
